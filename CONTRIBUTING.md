# CONTRIBUTING

- feel free to open issues or create merge requests
- please note that merge requests are not merged if
  - the change is not deemed useful by a maintainer (if there is no issue for it)
  - change requests are ignored or the CI/CD pipeline fails
- ideally join on Discord (see [https://sirimangalo.org/call-for-volunteers/](https://sirimangalo.org/call-for-volunteers/))
