FROM docker.io/library/node:22-alpine AS build

WORKDIR /app

# fix for https://vercel.com/guides/corepack-errors-github-actions
RUN npm install --global corepack@latest
RUN corepack enable
RUN apk --no-cache add curl bash jq yq

COPY package.json pnpm-lock.yaml .
COPY vendor ./vendor
RUN pnpm install --frozen-lockfile

COPY . .
RUN pnpm run build-scripts
RUN ./download-data.sh
RUN ./build-release.sh

FROM docker.io/library/nginx:1.27-alpine AS runtime

COPY --from=build /app/out /usr/share/nginx/html
