# Sirimangalo Podcasts

This repository contains the code for the Next.js web app available at [https://podcast.sirimangalo.org/](https://podcast.sirimangalo.org/) along with scripts for generating the associated RSS feeds.

## Architecture

- The website is currently being hosted as a static build via Gitlab Pages.
- Currently, a Google Sheet is being used for managing content (podcasts and their metadata). The sheet is [publicly available](https://docs.google.com/spreadsheets/d/1eW5UOStbGI5JzeUNk74onxPgcXlymfDX6Dca_E9Qekk/edit?gid=1556200646#gid=1556200646) (read-only) and must be downloaded and parsed before building the website.
- The audio files (and audio archive files) are stored in S3 Object Storage.
- RSS XML feeds are available for each podcast at `https://podcast.sirimangalo.org/<podcast-slug>/rss.xml`. These are also submitted to publisher platforms (Itunes, Spotify, Google). There is also an alternative json version `feed-data.json` for alternative use cases.

## Build Instructions

A local version can be build and run with Docker without additional tooling:

```bash
git clone git@gitlab.com:sirimangalo/podcast.git
cd podcast
cp .env.defaults .env
docker build -t podcast .
docker run --rm -p 3000:80 podcast
```

Open [http://localhost:3000](http://localhost:3000) (when using a different port, the `NEXT_PUBLIC_URL_BASE` variable in the `.env` file must be changed accordingly).

## Development

### Requirements

- [Node.js](https://nodejs.org/en) (>= 22)
- [pnpm](https://pnpm.io/)

**only needed for downloading the podcast feed data & build scripts:**

- [yq](https://github.com/mikefarah/yq)
- [jq](https://jqlang.github.io/jq/)
- curl
- Bash

### Quickstart

```bash
# create a local .env version
cp .env.defaults .env

pnpm install

# download the feed data into the data/ dir
./download-data.sh

pnpm dev
```

Open [http://localhost:3000](http://localhost:3000)

### Deployments

- Tags are created for each code release (naming: current day in YYYY-MM-DD; add `_2`, `_3` in case there are multiple releases per day)
- Rerun the CI/CD pipeline of the latest tag after changes to the spreadsheet were made. Check the build logs and the `changes/` folder in the artifact to avoid unintentional changes.

### Font Attribution

- RobotoSlab: https://fonts.google.com/specimen/Roboto+Slab (Apache License, Version 2.0)
- Charm: https://fonts.google.com/specimen/Charm (Open Font License)
- Material Icons: https://fonts.google.com/icons

