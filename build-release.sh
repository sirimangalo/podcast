#!/bin/bash

# Build RSS Feeds from data directory and build static website

set -e
set -u

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DATA_DIR="${DIR}/data"

. .env

pnpm run build-scripts

echo "=== Build & Validate Feeds ==="
for parsed in $DATA_DIR/feeds/*.json; do
  if [[ -e $parsed ]]; then
    echo $parsed
    node --env-file "${DIR}/.env" dist/scripts/create-feed.js -i $parsed -o ${parsed%.json}.xml
    node --env-file "${DIR}/.env" dist/scripts/validate-feed.js --strict -i ${parsed%.json}.xml
  fi
done

echo "=== Build Website ==="
pnpm run build

SITEMAP='./out/sitemap.txt'
touch $SITEMAP

for parsed in $DATA_DIR/feeds/*.xml; do
  if [[ -e $parsed ]]; then
    podcast_slug=$(basename $parsed)
    podcast_slug=${podcast_slug%.xml}

    echo "Copying Feed for ${podcast_slug}"
    cp $parsed "./out/${podcast_slug}/rss.xml"
    cp ${parsed%.xml}.json "./out/${podcast_slug}/feed-data.json"

    echo "${NEXT_PUBLIC_URL_BASE}/${podcast_slug}/" >> $SITEMAP
  fi
done
