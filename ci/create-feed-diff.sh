#!/bin/bash

# Check difference with currently published website by listing new and removed feeds
# and creating a html report displaying the changes in changed feeds.

set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DATA_DIR="${DIR}/../data"

TMPDIR="$(mktemp -d)"
echo "Entering $TMPDIR"
cd "$TMPDIR"

LATEST_PAGE_DIR="${TMPDIR}/current-page"
DIFF_FILE="${TMPDIR}/diff_files.txt" # file containing list of files to diff
DIST_DIR="${DIR}/../out"
CHANGES_DIR="${TMPDIR}/changes"
TXT_REPORT="${CHANGES_DIR}/files-changed.txt"

# download current website artifact
echo "=== Loading builds from latest artifact ==="
LATEST_VERSION=$(curl -s https://podcast.sirimangalo.org/version.txt)
curl -s "https://gitlab.com/sirimangalo/podcast/-/jobs/${LATEST_VERSION}/artifacts/download?file_type=archive" -L -o public.zip
mkdir -p ./unziptmp && unzip -qq ./public.zip -d ./unziptmp && mv ./unziptmp/public $LATEST_PAGE_DIR && rm -rf ./unziptmp

echo "=== Creating Feed Diff ==="
rm -f $DIFF_FILE
mkdir -p $CHANGES_DIR

# check removed feeds
echo "# Removed Feeds"
for parsed in $LATEST_PAGE_DIR/*/*.xml; do
  if [[ -e $parsed && ! -e ${DIST_DIR}${parsed#$LATEST_PAGE_DIR} ]]; then
    echo "- ${parsed#$LATEST_PAGE_DIR}"
  fi
done

echo "# Added Feeds"
for parsed in $DIST_DIR/*/*.xml; do
  if [[ -e $parsed ]]; then
    current_feed="${LATEST_PAGE_DIR}${parsed#$DIST_DIR}"

    if [[ ! -e "$current_feed" ]]; then
      echo " - ${parsed#$DIST_DIR}"
    fi

    cat "$parsed" | yq -p xml -o json | jq 'del(.rss.channel.lastBuildDate)' > new.json
    cat "$current_feed" | yq -p xml -o json | jq 'del(.rss.channel.lastBuildDate)' > old.json
    diff old.json new.json > diff.txt || true

    num_lines=$(wc -l diff.txt | cut -d' ' -f1)
    if [[ "$num_lines" -ne "0" ]]; then
      diff_file=$(echo "${parsed#$DIST_DIR}.diff.txt" | sed 's#/#_#g')
      mv diff.txt "${CHANGES_DIR}/$diff_file"
    fi

    rm -f old.json new.json diff.txt
  fi
done

echo "# Feed Changes"
diff -qr "${LATEST_PAGE_DIR}" "${DIST_DIR}" > $TXT_REPORT || true

if [[ $CI ]]; then
  echo "Moving changes dir"
  mv "$CHANGES_DIR" "$DIR/../"
fi
