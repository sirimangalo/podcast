#!/bin/bash

# Download Data from Spreadsheet and create JSON files

set -e # exit on errors
set -u

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DATA_DIR="${DIR}/data/feeds"

. .env

mkdir -p "${DATA_DIR}"

TMPDIR="$(mktemp -d)"
cd "${TMPDIR}"

echo "Entering ${TMPDIR}"

echo "=== Downloading metadata ==="
STORAGE_HOST="$(echo "${STORAGE_URL}" | sed 's|digitaloceanspaces\.com.*$||g')digitaloceanspaces.com"
STORAGE_PREFIX=${STORAGE_URL#"${STORAGE_HOST}/"}

# the s3 endpoint only returns max. 1000 items per request
# pagination must be handled...
marker=''
tmpfile="$(mktemp)"
for i in {1..40}
do
  xml=$(curl -s "${STORAGE_HOST}?prefix=${STORAGE_PREFIX}&marker=${marker}")
  marker=$(echo "$xml" | yq -p xml -r '.ListBucketResult.NextMarker')

  # convert to yaml temporarily because concatenating array is easy
  echo "$xml" | yq -p xml -o yaml '.ListBucketResult.Contents' >> "${tmpfile}"

  if [ "${marker}" = "null" ]; then
    break
  fi
done

cat "${tmpfile}" | yq -p yaml -o json | jq 'map( {(.Key): .} ) | add' > ./metadata.json
rm -f "${tmpfile}"

curl -s "${STORAGE_HOST}?prefix=${STORAGE_PREFIX}/archives" | yq -p xml -o json | jq '.ListBucketResult.Contents | map( {(.Key): .} ) | add' > ./metadata-archives.json

curl -L -s -o ./durations.json "https://gitlab.com/sirimangalo/podcast-data/-/raw/master/storage/durations.json"

echo "=== Downloading sheet ==="
curl -L -s "https://docs.google.com/spreadsheets/d/${DATA_SHEET_ID}/export?format=xlsx&id=${DATA_SHEET_ID}" -o ./data.xlsx

echo "=== Parsing sheet ==="
pnpm run -C "$DIR" build-scripts
node --env-file "${DIR}/.env" "$DIR/dist/scripts/parse-sheet.js" -i data.xlsx -m ./metadata.json -d ./durations.json -o $DATA_DIR

mv ./metadata-archives.json "${DATA_DIR}/.."

# cleaning up
rm -rf "${TMPDIR}"
