import path from 'node:path';
import CopyPlugin from 'copy-webpack-plugin';
import type { NextConfig } from 'next';

const nextConfig: NextConfig = {
  output: 'export',
  basePath: process.env.NEXT_PUBLIC_URL_BASE?.startsWith(
    'https://sirimangalo.gitlab.io',
  )
    ? process.env.NEXT_PUBLIC_URL_BASE.replace(
        'https://sirimangalo.gitlab.io',
        '',
      )
    : '',
  trailingSlash: true,
  webpack: (config) => {
    config.plugins.push(
      new CopyPlugin({
        patterns: [
          {
            from: path.join(__dirname, 'node_modules/shikwasa/dist/style.css'),
            to: path.join(__dirname, 'public/css/shikwasa.css'),
          },
        ],
      }),
    );

    return config;
  },
};

export default nextConfig;
