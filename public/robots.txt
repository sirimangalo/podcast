User-agent: *
Disallow: /_next/
Disallow: /404/
Disallow: /img/
Disallow: /sitemap.xml
Disallow: /sitemap.txt
Disallow: /favicon.ico
Disallow: /version.txt
