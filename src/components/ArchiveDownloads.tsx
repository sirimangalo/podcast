import type { PodcastArchive, PodcastProp } from '@/interfaces';
import { formatFileSize } from '../lib/utils';

export const ArchiveDownloads = ({
  archives,
}: { archives: PodcastProp['archives'] }) => (
  <div className="dropdown is-hoverable">
    <div className="dropdown-trigger">
      <button
        type="button"
        className="button is-primary"
        aria-haspopup="true"
        aria-controls="dropdown-menu"
      >
        <span className="icon is-small">
          <i className="material-icons">download</i>
        </span>
        <span>Download Podcast</span>
      </button>
    </div>
    <div className="dropdown-menu" id="dropdown-menu" role="menu">
      <div className="dropdown-content">
        {archives.map(({ downloadUrl, format, bitrate, Size: fileSize }) => (
          <a href={downloadUrl} className="dropdown-item" key={downloadUrl}>
            <div className="dropdown-item-download">
              <span>
                {format?.toUpperCase()}, {bitrate} kbps{' '}
              </span>
              <span>{formatFileSize(Number.parseInt(fileSize), 1)}</span>
            </div>
          </a>
        ))}
      </div>
    </div>
  </div>
);
