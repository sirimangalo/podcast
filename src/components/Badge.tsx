import React from 'react';
import styles from './Badge.module.css';

const BADGE_TYPES = ['Itunes', 'Spotify', 'Google'];
const BADGE_IMAGES: Record<(typeof BADGE_TYPES)[number], string> = {
  Itunes: '/img/itunes-badge.png',
  Google: '/img/yt-music-badge.png',
  Spotify: '/img/spotify-badge.png',
};

const Badge = ({ type, link }: { type: string; link: string }) =>
  BADGE_TYPES.includes(type) && link ? (
    <a href={link} target="_blank" rel="noopener noreferrer">
      <img
        className={styles.badge}
        src={`${process.env.NEXT_PUBLIC_URL_BASE}${BADGE_IMAGES[type]}`}
        alt={type}
      />
    </a>
  ) : null;

export default Badge;
