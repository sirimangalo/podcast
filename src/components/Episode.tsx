import { unescape as unescapeHe } from 'he';
import React from 'react';
import styles from './Episode.module.css';
import Tooltip from './Tooltip';

import type {
  EpisodeProp,
  Episode as EpisodeType,
  PlayingEpisode,
} from '../interfaces';

import {
  formatDuration,
  formatFileSize,
  getEpisodeAnchor,
  parsePubDate,
} from '../lib/utils';

const stripCdata = (s: string): string =>
  s.replace('<![CDATA[', '').replace(']]>', '');

type Props = {
  data: EpisodeProp &
    Pick<EpisodeType, 'description' | 'contentEncoded'> & {
      podcastId: string;
      podcastTitle: string;
      podcastCoverUrl: string;
    };
  isSerial: boolean;
  onPlay: (episode: PlayingEpisode) => void;
};

type State = {
  showFullDesc: boolean;
  linkCopied: boolean;
};

class Episode extends React.Component<Props, State> {
  state = {
    showFullDesc: false,
    linkCopied: false,
  };

  constructor(props: Props) {
    super(props);

    // @ts-ignore
    this.titleRef = React.createRef();
  }

  handleToggleDesc = () =>
    this.setState(
      {
        showFullDesc: !this.state.showFullDesc,
      },
      // @ts-ignore
      () => this.state.showFullDesc && this.titleRef.current.scrollIntoView(),
    );

  handleEpisodeCopyLink = (id: string) => {
    const url = window.location.href.split(/[?#]/)[0];

    this.copyToClipboard(`${url}#${id}`);
  };

  copyToClipboard = (text: string) => {
    // create a temporary input element to execute copy command
    const input = document.createElement('textarea');
    input.value = text;
    input.style.position = 'fixed';

    document.body.appendChild(input);
    input.focus();
    input.select();
    try {
      document.execCommand('copy');
    } catch (err) {
      console.error(err);
      throw err;
    } finally {
      document.body.removeChild(input);
    }
  };

  render() {
    // @ts-ignore
    const { isSerial, data, onPlay } = this.props;

    const { showFullDesc, linkCopied } = this.state;

    const title = unescapeHe(data?.title ?? 'Untitled', {
      // @ts-ignore
      useNamedReferences: true,
    });

    const contentEncoded = stripCdata(data?.contentEncoded ?? '');
    const pubDate = parsePubDate(data.pubDate).format('LL');
    const anchorLink = getEpisodeAnchor(data);

    return (
      <div
        className={styles.episode}
        // @ts-ignore
        id={anchorLink || null}
        // @ts-ignore
        ref={this.titleRef}
      >
        <p className={styles.pubdate}>
          {isSerial ? `Episode ${data.episode}` : pubDate}
        </p>
        <h3 className={styles.title}>
          {anchorLink ? (
            <>
              {title}&nbsp;
              <Tooltip
                text={linkCopied ? 'Copied!' : 'Copy episode link'}
                onMouseEnter={() => this.setState({ linkCopied: false })}
                onClick={(e) => {
                  e.preventDefault();
                  try {
                    this.handleEpisodeCopyLink(anchorLink);
                  } catch (err) {
                    return console.error(err);
                  }
                  this.setState({ linkCopied: true });
                }}
              >
                <a href={`#${anchorLink}`}>
                  <i className="material-icons">link</i>
                </a>
              </Tooltip>
            </>
          ) : (
            title
          )}
        </h3>

        {showFullDesc && (
          <div
            className={styles.description}
            dangerouslySetInnerHTML={{ __html: contentEncoded }}
          />
        )}

        <div className={styles.actions}>
          <noscript>
            <a href={data.audioUrl} target="_blank" rel="noopener noreferrer">
              Play
            </a>
          </noscript>
          <button
            type="button"
            className={`button ${styles.play} is-primary`}
            onClick={() => onPlay(data)}
          >
            <i className="material-icons">play_circle_filled</i>
            &nbsp; Play
          </button>
          {/* BUG: Does not download file directly, but opens in a new tab due to not same origin and missing content-disposition header in response. */}
          <a
            href={data.audioUrl}
            target="blank"
            rel="nofollow noreferrer"
            download
          >
            <button
              type="button"
              className={`button ${styles.play} is-primary`}
            >
              <i className="material-icons">download</i>
              &nbsp; Download &nbsp;
              <span>({formatFileSize(data.audioSize, 0)})</span>
            </button>
          </a>
          {` ${formatDuration(data?.duration ?? 0)}`}
          <div className="flex-fill" />
          <i
            className={`${styles.info} material-icons clickable`}
            onClick={this.handleToggleDesc}
          >
            {showFullDesc ? 'highlight_off' : 'info'}
          </i>
        </div>
      </div>
    );
  }
}

export default Episode;
