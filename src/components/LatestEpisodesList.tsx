import { formatDuration, getSmallArtworkUrl, parsePubDate } from '@/lib/utils';
import { unescape as unescapeHe } from 'he';
import Link from 'next/link';
import React, { useState, useRef } from 'react';
import TouchCarousel from 'react-touch-carousel';
import touchWithMouseHOC from 'react-touch-carousel/lib/touchWithMouseHOC';

import styles from './LatestEpisodesList.module.css';

const CARD_SIZE = 313; // px

const LatestEpisodeContainer = touchWithMouseHOC((props) => {
  const { className, carouselState, cursor, ...additionalProps } = props;
  const translateX = cursor * CARD_SIZE;

  return (
    <div className={styles['latest-episode-container']}>
      <div
        className={`
        ${styles['carousel-track']}
        ${carouselState.dragging ? styles['is-dragging'] : ''}
        ${className || ''}
        `}
        style={{ transform: `translate3d(${translateX}px, 0, 0)` }}
        {...additionalProps}
      />
    </div>
  );
});

const LatestEpisodeItem = ({ episode }) => {
  return (
    <div className={`${styles['latest-episode-item']} card p-4`}>
      <div className="media mb-4">
        <div className="media-left">
          <div>
            <figure className={styles.image}>
              <img src={getSmallArtworkUrl(episode.imageUrl)} />
            </figure>
          </div>
        </div>

        <div className={styles['media-content']}>
          <p className="title mr-5 mb-0 is-size-7">
            {unescapeHe(episode.podcastName)}
          </p>
          <p>{parsePubDate(episode.pubDate).format('LL')}</p>
        </div>
      </div>

      <h4 className={`${styles.title} mr-4 mb-2 pb-1`}>
        {unescapeHe(episode.title)}
      </h4>

      <Link href={episode.link}>
        <button
          type="button"
          className={`${styles['play-timestamp']} button is-primary`}
          title="Play"
        >
          <i className="material-icons is-size-5 mr-1">play_circle_filled</i>
          <span>{formatDuration(episode.duration)}</span>
        </button>
      </Link>

      <a
        className="ml-1"
        style={{ color: 'currentColor' }}
        href={episode.audioUrl}
        target="blank"
        rel="nofollow noreferrer"
        download
      >
        <button
          type="button"
          className={`${styles['download-btn']} button is-primary`}
          title="Download"
        >
          <i className="material-icons is-size-5">download</i>
        </button>
      </a>
    </div>
  );
};

const LatestEpisodesList = ({ data, collapsed, ...additionalProps }) => {
  const carousel = useRef();
  const [arrowDisabled, setArrowDisabled] = useState(null);
  data = data || [];

  const moveNext = () => {
    // @ts-ignore
    const cursorPos = carousel?.current?.state?.cursor;
    if (cursorPos <= -(data.length - 1)) return;
    // @ts-ignore
    carousel.current.next();
  };

  const movePrev = () => {
    // @ts-ignore
    const cursorPos = carousel.current.state.cursor;
    if (cursorPos > -1) return;
    // @ts-ignore
    carousel.current.prev();
  };

  const disableArrowOnCarouselEnd = () => {
    // @ts-ignore
    const cursorPos = carousel.current.state.cursor;
    if (cursorPos >= 0) setArrowDisabled('LEFT');
    else if (cursorPos <= -(data.length - 1)) setArrowDisabled('RIGHT');
    else setArrowDisabled(null);
  };

  return (
    <div
      className={`${styles.collapsible} ${collapsed ? styles.collapsed : ''}`}
    >
      <div className="container is-relative is-unselectable">
        {/* TITLE SECTION */}
        <div className="is-flex is-justify-content-space-between is-align-items-flex-end mt-5 mb-4">
          <h1 className="podcast-group-title">Latest Episodes</h1>

          <div>
            {/* LEFT & RIGHT ARROW BUTTONS */}
            <button
              type="button"
              className={styles['carousel-btn-left']}
              disabled={arrowDisabled === 'LEFT'}
              onClick={movePrev}
            >
              <i className="material-icons is-size-5">arrow_back</i>
            </button>

            <span className="ml-1" />

            <button
              type="button"
              className={styles['carousel-btn-right']}
              disabled={arrowDisabled === 'RIGHT'}
              onClick={moveNext}
            >
              <i className="material-icons is-size-5">arrow_forward</i>
            </button>
          </div>
        </div>

        {/* CAROUSEL */}
        <TouchCarousel
          component={LatestEpisodeContainer}
          renderCard={(index) => (
            <LatestEpisodeItem key={index} episode={data[index]} />
          )}
          cardSize={CARD_SIZE}
          cardCount={data.length}
          loop={false}
          onRest={disableArrowOnCarouselEnd}
          ref={carousel}
          {...additionalProps}
        />
      </div>
    </div>
  );
};

export default LatestEpisodesList;
