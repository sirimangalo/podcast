import throttle from 'lodash/throttle';
import React, { useEffect, useState } from 'react';
import styles from './ScrollButton.module.css';

const ScrollButton = ({ isPlaying }) => {
  const [showScroll, setShowScroll] = useState(false);

  useEffect(function onFirstMount() {
    window.addEventListener('scroll', onScroll);

    return () => {
      window.removeEventListener('scroll', onScroll);
    };
  }, []);

  const onScroll = throttle(() => {
    if (window.pageYOffset > 400) {
      setShowScroll(true);
    }
    if (window.pageYOffset <= 400) {
      setShowScroll(false);
    }
  }, 500);

  const scrollTop = () => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  };

  return (
    <button
      type="button"
      className={
        showScroll ? styles.scrollButtonShown : styles.scrollButtonHidden
      }
      title="Go to top"
      onClick={scrollTop}
      style={isPlaying ? { bottom: '150px' } : {}}
    >
      <i className="material-icons">arrow_upward</i>
    </button>
  );
};

export default ScrollButton;
