import type { Podcast } from '@/interfaces';
import React from 'react';
import styles from './SubscribeButton.module.css';

type Props = Pick<Podcast, 'title' | 'imageUrl' | 'itunesLink'> & {
  feedUrl: string;
};

class SubscribeButton extends React.Component<Props> {
  componentDidMount() {
    const { title, imageUrl, feedUrl, itunesLink } = this.props;

    if (typeof window === 'undefined') {
      return;
    }

    const dataKey = `podcastData-${feedUrl}`;
    window[dataKey] = {
      title: title,
      subtitle: '',
      description: '',
      cover: imageUrl,
      feeds: [
        {
          type: 'audio',
          format: 'mp3',
          url: feedUrl,
          'directory-url-itunes': itunesLink,
        },
      ],
    };

    const script = document.createElement('script');
    script.async = true;
    script.src = 'https://cdn.podlove.org/subscribe-button/javascripts/app.js';

    script.setAttribute('class', 'podlove-subscribe-button');
    script.setAttribute('data-language', 'en');
    script.setAttribute('data-size', 'medium');
    script.setAttribute('data-json-data', dataKey);
    script.setAttribute('data-color', '#469cd1');
    script.setAttribute('data-format', 'rectangle');
    script.setAttribute('data-style', 'filled');

    // @ts-ignore
    this.span.appendChild(script);
  }

  render() {
    const { feedUrl } = this.props;

    return (
      <div className={styles.wrapper}>
        <span
          ref={(el) => {
            // @ts-ignore
            this.span = el;
          }}
        >
          <noscript>
            <a href={feedUrl}>Subscribe</a>
          </noscript>
        </span>
      </div>
    );
  }
}

export default SubscribeButton;
