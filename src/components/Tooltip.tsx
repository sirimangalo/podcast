import type React from 'react';
import { type DOMAttributes, useState } from 'react';
import styles from './Tooltip.module.css';

const Tooltip = ({
  children,
  text,
  onMouseEnter,
  onMouseLeave,
  ...additionalProps
}: {
  children: React.ReactNode;
  text: string;
} & Partial<DOMAttributes<HTMLDivElement>>) => {
  const [show, setShow] = useState(false);

  const handleMouseEnter: React.MouseEventHandler<HTMLDivElement> = (e) => {
    setShow(true);
    if (onMouseEnter) onMouseEnter(e);
  };

  const handleMouseLeave: React.MouseEventHandler<HTMLDivElement> = (e) => {
    setShow(false);
    if (onMouseLeave) onMouseLeave(e);
  };

  return (
    <div className={styles.container}>
      <div className={`${styles.tooltip} ${show && styles.visible}`}>
        {text}
      </div>

      <div
        onMouseEnter={handleMouseEnter}
        onMouseLeave={handleMouseLeave}
        {...additionalProps}
      >
        {children}
      </div>
    </div>
  );
};

export default Tooltip;
