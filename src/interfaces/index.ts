export interface Episode {
  guid: string;
  title: string;
  pubDate: string;
  description: string;
  audioUrl: string;
  audioSize: number;
  audioType: string;
  contentEncoded?: string;
  subtitle?: string;
  link?: string;
  duration?: number;
  categories?: string[];
  episode?: number;
  season?: number;
  chapters?: { start: string; title: string; href?: string }[];
}

export interface Podcast {
  title: string;
  description: string;
  imageUrl: string;
  category: string;
  subcategory: string;
  author: string;
  ownerName: string;
  ownerEmail: string;
  episodes: Episode[];
  lastBuildDate?: string;
  link?: string;
  itunesLink?: string;
  spotifyLink?: string;
  googleLink?: string;
  atomLink?: string;
  language?: string;
  type?: 'episodic' | 'serial';
  complete?: 'Yes' | 'No';
}

export type EpisodeProp = Pick<
  Episode,
  | 'guid'
  | 'title'
  | 'audioSize'
  | 'audioUrl'
  | 'episode'
  | 'duration'
  | 'pubDate'
  | 'link'
>;

export type S3ObjectMetadata = {
  Key: string;
  LastModified: string;
  ETag: string;
  Size: string;
  StorageClass: string;
  Owner: {
    ID: string;
    DisplayName: string;
  };
  Type: string;
};

export type PodcastArchive = {
  downloadUrl: string;
  bitrate: number;
  format: 'mp3' | 'opus';
};

export type PodcastProp = Omit<Podcast, 'episodes'> & {
  episodes: EpisodeProp[];
  archives: (PodcastArchive & S3ObjectMetadata)[];
};

export type PodcastMetadata = Omit<Podcast, 'episodes'> & {
  id: string;
  lastEpisodeDate: Date;
  numEpisodes: number;
};

export type EpisodeWithPodcastMetadata = Episode & {
  imageUrl: string;
  podcastName: string;
  podcastId: string;
};

export type PlayingEpisode = Pick<Episode, 'title' | 'audioUrl'> & {
  podcastTitle: string;
  podcastId: string;
  podcastCoverUrl: string;
};
