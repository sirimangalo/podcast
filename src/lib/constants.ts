export const SORT_ASC = 'ASC';
export const SORT_DESC = 'DESC';

export const SORT_BY_TITLE = 'title';
export const SORT_BY_DATE = 'lastEpisodeDate';

export const MAX_META_DESC_SIZE = 155;
export const MAX_SHORT_DESC_SIZE = 140;

export const COUNT_LATEST_EPISODES = 10;

// Grouping of podcasts by matching the slug.
// The order of the items reflects the order
// it should be shown on the page.
// The wildcard group 'other' MUST be the last one.
export const PODCAST_GROUPS = [
  {
    id: 'questions',
    pattern: /^(dhamma-qa|ask-a-monk|audio-book-qa).*$/,
  },
  {
    id: 'dhammapada',
    pattern: /^dhammapada.*$/,
  },
  {
    id: 'how-to-meditate',
    pattern: /^how-to-meditate.*$/,
  },
  {
    id: 'dhamma-talks',
    pattern: /^(daily-dhamma-2015|thai-talks|la-course-talks|dhammatalks.*)$/,
  },
  {
    id: 'text-studies',
    pattern: /^(jataka-stories|sutta-talks|visuddhimagga)$/,
  },
  {
    id: 'other',
    pattern: /^.+$/,
  },
] as const;

export const PODCAST_GROUP_NAMES = {
  questions: 'Questions & Answers',
  dhammapada: 'Dhammapada',
  'how-to-meditate': 'How To Meditate',
  other: 'Other Podcasts',
  'dhamma-talks': 'Damma Talks',
  'text-studies': 'Text-Studies',
} as const;

// ref: https://gist.github.com/MatthewBarker/25f21f70d5f98a71fa737d94010eec65
export const RSS_DATE_FORMATS = [
  'ddd, DD MMM YYYY HH:mm:ss ZZ',
  'ddd, DD MMM YY HH:mm:ss ZZ',
];
