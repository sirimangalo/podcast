import path from 'node:path';
import { expect, test } from 'vitest';
import { loadAllPodcasts } from './data';

test('loads all podcasts from filesystem', () => {
  const podcasts = loadAllPodcasts();
  expect(podcasts.length).toBeGreaterThan(20);
  expect(podcasts[0][0]).toBeTypeOf('string');
  expect(podcasts[0][1]).toHaveProperty('title');
  expect(podcasts[0][1]).toHaveProperty('episodes');
  expect(podcasts[0][1]).toHaveProperty('author');
});
