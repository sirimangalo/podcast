import { readFileSync, readdirSync } from 'node:fs';
import path from 'node:path';
import * as pako from 'pako';
import type {
  Episode,
  EpisodeProp,
  Podcast,
  PodcastArchive,
  PodcastProp,
} from '../interfaces';
import { pick } from './utils';

export const loadAllPodcastIds = (): string[] =>
  readdirSync(path.join(process.cwd(), 'data', 'feeds'))
    .filter((fn) => fn.endsWith('.json'))
    .map((fn) => fn.replace('.json', ''));

export const loadAllPodcasts = (select?: string[]): [string, Podcast][] =>
  loadAllPodcastIds()
    .filter((fn) => !select || select.includes(fn))
    .map((fn) => [
      fn,
      JSON.parse(
        readFileSync(
          path.join(process.cwd(), `/data/feeds/${fn}.json`),
          'utf8',
        ),
      ) as Podcast,
    ]);

export const loadMetadata = () => {
  return JSON.parse(
    readFileSync(
      path.join(process.cwd(), 'data', 'metadata-archives.json'),
      'utf-8',
    ),
  );
};

export const loadDataForPodcastPage = (
  podcastId: string,
  storageUrl: string,
): {
  props: {
    data: PodcastProp;
    descriptions: string;
  };
} => {
  const storageBaseUrl = `https://${new URL(storageUrl).hostname}`;
  const podcastData = loadAllPodcasts([podcastId])[0][1];

  const metadataArchives = loadMetadata();

  if (podcastData.episodes && podcastData.type === 'serial') {
    podcastData.episodes.sort((a, b) => {
      const seasonA = a.season ?? 0;
      const seasonB = b.season ?? 0;

      if (seasonA !== seasonB) {
        return seasonA - seasonB;
      }

      const episodeA = a?.episode ?? 0;
      const episodeB = b?.episode ?? 0;

      return episodeA - episodeB;
    });
  }

  const descriptions = podcastData.episodes.reduce(
    (obj, { guid, description, contentEncoded }) => {
      obj[guid] = { description, contentEncoded };
      return obj;
    },
    {} as Record<string, Pick<Episode, 'description' | 'contentEncoded'>>,
  );

  const episodes: EpisodeProp[] = podcastData.episodes.map((ep) =>
    pick(ep, [
      'guid',
      'title',
      'audioSize',
      'audioUrl',
      'episode',
      'duration',
      'pubDate',
      'link',
    ] as (keyof Episode)[]),
  );

  return {
    props: {
      data: {
        ...podcastData,
        episodes,
        archives: (
          [
            {
              downloadUrl: `podcast-mirror/archives/${podcastId}.zip`,
              bitrate: 128,
              format: 'mp3',
            },
            {
              downloadUrl: `podcast-mirror/archives/${podcastId}_opus_32.zip`,
              bitrate: 32,
              format: 'opus',
            },
            {
              downloadUrl: `podcast-mirror/archives/${podcastId}_opus_16.zip`,
              bitrate: 16,
              format: 'opus',
            },
            {
              downloadUrl: `podcast-mirror/archives/${podcastId}_opus_10.zip`,
              bitrate: 10,
              format: 'opus',
            },
          ] as Pick<PodcastArchive, 'downloadUrl' | 'bitrate' | 'format'>[]
        )
          .filter(({ downloadUrl }) => metadataArchives[downloadUrl])
          .map(({ downloadUrl, bitrate, format }) => ({
            ...metadataArchives[downloadUrl],
            format,
            bitrate,
            downloadUrl: `${storageBaseUrl}/${downloadUrl}`,
          })),
      },
      // compress descriptions because they take up a lot of size
      // and contain many recurring patterns
      descriptions: pako.deflate(JSON.stringify(descriptions), {
        to: 'string',
      }),
    },
  };
};
