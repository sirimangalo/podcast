import type { Episode } from '@/interfaces';
import ISO6391 from 'iso-639-1';
import moment from 'moment';
import { RSS_DATE_FORMATS } from './constants';

export const parsePubDate = (pubDate: string): moment.Moment =>
  moment(pubDate, RSS_DATE_FORMATS);

/**
 * Get anchor link for an episode
 *
 * @param {object} episode an episode object
 */
export const getEpisodeAnchor = (episode: Pick<Episode, 'link'>): string => {
  const parts = episode?.link?.split('#');
  return parts?.length ? parts[parts.length - 1] : '';
};

/**
 * Get the full name of a two letter language code.
 *
 * @param {string} isoCode  iso code
 */
export const getLanugageName = (
  isoCode: string,
  unknownName = 'Unknown',
): string => {
  if (!isoCode) {
    return unknownName;
  }

  const language = ISO6391.getName(isoCode.slice(0, 2));
  return language ? language : unknownName;
};

/**
 * Get an url to a smaller version of the artwork (300x300 pixels).
 *
 * @param {string} url  artwork url
 */
export const getSmallArtworkUrl = (url: string): string => {
  const splitted = url.split('/');
  return [
    ...splitted.slice(0, -1),
    splitted[splitted.length - 1].replace('artwork-', 'artwork-300x300-'),
  ].join('/');
};

/**
 * Format duration in seconds as string.
 *
 * @param {number} secondsRaw
 */
export const formatDuration = (secondsRaw: number): string => {
  const hours = Math.floor(secondsRaw / 3600);
  const minutes = Math.floor((secondsRaw - hours * 3600) / 60);
  const seconds = secondsRaw - hours * 3600 - minutes * 60;

  const hoursStr = hours < 10 ? `0${hours}` : hours.toString();
  const minutesStr = minutes < 10 ? `0${minutes}` : minutes.toString();
  const secondsStr = seconds < 10 ? `0${seconds}` : seconds.toString();

  return `${hoursStr}:${minutesStr}:${secondsStr}`;
};

export const formatFileSize = (bytes: number, decimals = 2): string => {
  if (!bytes) return '0 B';
  const sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
  const i = Math.floor(Math.log(bytes) / Math.log(1024));
  return `${Number.parseFloat((bytes / 1024 ** i).toFixed(decimals))} ${sizes[i]}`;
};

/**
 * Compare a list of episodes
 *  - by date
 *  - by episode if same podcast
 * in descending order.
 *
 * @param {object} epA    Episode A (with additional 'podcastId' property)
 * @param {object} epB    Episode B (with additional 'podcastId' property)
 * @returns               -1, 1 or zero according to comparison result.
 */
export const latestEpisodeComparator = (
  epA: Episode & { podcastId: string },
  epB: Episode & { podcastId: string },
): -1 | 0 | 1 => {
  const pubDateA = parsePubDate(epA.pubDate);
  const pubDateB = parsePubDate(epB.pubDate);

  if (!pubDateA.isValid()) {
    return 1;
  }

  if (!pubDateB.isValid()) {
    return -1;
  }

  if (pubDateA.isSame(pubDateB)) {
    if (
      epA.podcastId &&
      epA.podcastId === epB.podcastId &&
      epA.episode &&
      epB.episode
    ) {
      return epA.episode > epB.episode ? -1 : 1;
    }

    return 0;
  }

  return pubDateA.isAfter(pubDateB) ? -1 : 1;
};

// pick utility that omits undefined fields
export const pick = <T, K extends keyof T>(obj: T, keys: K[]): Pick<T, K> => {
  const ret: Partial<Pick<T, K>> = {};
  keys.forEach((key) => {
    if (typeof obj[key] === 'undefined') return;
    ret[key] = obj[key];
  });
  return ret as Pick<T, K>;
};
