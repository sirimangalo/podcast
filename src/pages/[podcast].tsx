import { ArchiveDownloads } from '@/components/ArchiveDownloads';
import Badge from '@/components/Badge';
import Episode from '@/components/Episode';
import SubscribeButton from '@/components/SubscribeButton';
import type {
  Episode as EpisodeType,
  PlayingEpisode,
  PodcastProp,
} from '@/interfaces';
import { MAX_META_DESC_SIZE, MAX_SHORT_DESC_SIZE } from '@/lib/constants';
import { loadAllPodcastIds, loadDataForPodcastPage } from '@/lib/data';
import { getSmallArtworkUrl } from '@/lib/utils';
import { unescape as unescapeHe } from 'he';
import Head from 'next/head';
import Link from 'next/link';
import { type Router, withRouter } from 'next/router';
import * as pako from 'pako';
import React from 'react';

type Props = {
  data: PodcastProp;
  descriptions: string; // compressed
  onPlay: (
    episode: PlayingEpisode,
    nextEpisodes: Pick<PodcastProp['episodes'][number], 'title' | 'audioUrl'>[],
  ) => void;
  router: Router;
};

type State = {
  showFullDesc: boolean;
  currentSearch: string;
  descriptions: {
    [guid: string]: Pick<EpisodeType, 'description' | 'contentEncoded'>;
  };
};

class Podcast extends React.Component<Props, State> {
  state = {
    showFullDesc: false,
    currentSearch: '',
    descriptions: {},
  };

  componentDidMount() {
    this.populateDescriptions();
    this.formatLegacyLinks();
    this.openEpisodeOnAnchor();
  }

  populateDescriptions = () => {
    try {
      const descriptions = JSON.parse(
        pako.inflate(this.props.descriptions, { to: 'string' }),
      );
      this.setState({ descriptions });
    } catch (e) {
      console.error('Could not uncompress descriptions', e);
    }
  };

  formatLegacyLinks = () => {
    if (window.location.hash?.startsWith('#/')) {
      window.location.hash = `#${window.location.hash.slice(2)}`;
    }
  };

  openEpisodeOnAnchor = () => {
    // open and load audio player when URL contains an episode anchor link (e.g. #QoqSU8zZUSg)
    const hash = window.location.hash.slice(1);
    if (!hash) return;

    const { onPlay, data, router } = this.props;
    const podcastTitle = unescapeHe(data?.title ?? '');
    const podcastCoverUrl = data?.imageUrl;
    const podcastId = router.query.podcast as string;
    const episodes = data.episodes ?? [];

    for (const episode of episodes) {
      const { guid } = episode;

      if (hash === guid.replace(/\//g, '_')) {
        onPlay(
          {
            ...episode,
            podcastTitle,
            podcastId,
            podcastCoverUrl,
          },
          [],
        );
        break;
      }
    }
  };

  handleToggleDesc = () => {
    this.setState(({ showFullDesc }) => ({ showFullDesc: !showFullDesc }));
  };

  handleSearchChange = (e) => this.setState({ currentSearch: e.target.value });

  render() {
    const { showFullDesc, currentSearch, descriptions } = this.state;

    const { router, data, onPlay } = this.props;

    const podcastId = router.query.podcast;

    let episodes = data.episodes ?? [];

    if (currentSearch) {
      episodes = episodes.filter((x) =>
        x.title.toLowerCase().includes(currentSearch.toLowerCase()),
      );
    }

    const title = unescapeHe(data?.title ?? '');
    const description = unescapeHe(data?.description ?? '');
    const imageUrl = data?.imageUrl ?? '';
    const itunesLink = data?.itunesLink ?? '';
    const spotifyLink = data?.spotifyLink ?? '';
    const googleLink = data?.googleLink ?? '';
    const archives = data?.archives ?? [];
    const isSerial = data?.type === 'serial';

    const feedUrl = `${process.env.NEXT_PUBLIC_URL_BASE}/${podcastId}/rss.xml`;

    const metaDesc =
      description.length > MAX_META_DESC_SIZE
        ? `${description.slice(0, MAX_META_DESC_SIZE)}...`
        : description;

    return (
      <div id="podcast">
        <Head>
          <meta name="description" content={metaDesc} />
          <link
            type="application/rss+xml"
            rel="alternate"
            title={title}
            href={feedUrl}
          />
          <title>{title}</title>
        </Head>

        <div className="header-section-small" />

        <div className="is-clipped">
          <div style={{ padding: '20px' }}>
            <Link href="/">
              <button
                type="button"
                className="button"
                style={{
                  backgroundColor: 'transparent',
                  border: '1px solid transparent',
                  boxShadow: 'none',
                }}
              >
                <i className="material-icons">keyboard_backspace</i>
                &nbsp; Podcast Overview
              </button>
            </Link>
          </div>

          <div className="container is-max-desktop">
            <div className="columns">
              <div className="column is-one-third">
                <div className="title-section">
                  {imageUrl && (
                    <a href={process.env.NEXT_PUBLIC_URL_BASE}>
                      <img
                        className="avatar"
                        src={getSmallArtworkUrl(imageUrl)}
                        alt={title}
                      />
                    </a>
                  )}
                  <h1>{title}</h1>
                  <p>
                    {description && description.length > MAX_SHORT_DESC_SIZE ? (
                      <>
                        {!showFullDesc
                          ? `${description.slice(0, MAX_SHORT_DESC_SIZE)}... `
                          : `${description} `}
                        <a
                          className="clickable"
                          onClick={this.handleToggleDesc}
                        >
                          show {!showFullDesc ? 'more' : 'less'}
                        </a>
                      </>
                    ) : (
                      <>{description}</>
                    )}
                  </p>
                  {archives?.length ? (
                    <div className="download-podcast">
                      <ArchiveDownloads archives={archives} />
                    </div>
                  ) : null}
                  <hr />
                  <div className="subscribe-buttons">
                    <SubscribeButton
                      title={title}
                      imageUrl={getSmallArtworkUrl(imageUrl)}
                      feedUrl={feedUrl}
                    />
                    <Badge type="Itunes" link={itunesLink} />
                    <Badge type="Google" link={googleLink} />
                    <Badge type="Spotify" link={spotifyLink} />
                  </div>
                </div>
              </div>
              <div className="column">
                <div className="content-section">
                  <input
                    className="input"
                    placeholder="Search Episodes"
                    type="text"
                    value={currentSearch}
                    onChange={this.handleSearchChange}
                  />
                  {currentSearch && <h6>{episodes.length} Hits</h6>}
                  {episodes.map((data, i) => (
                    <div
                      key={`episode-${data.guid}`}
                      id={`episode${data?.guid ?? ''}`}
                    >
                      <Episode
                        data={{
                          ...data,
                          ...descriptions?.[data.guid],
                          podcastId,
                          podcastTitle: title,
                          podcastCoverUrl: imageUrl,
                        }}
                        isSerial={isSerial}
                        onPlay={(ep) =>
                          onPlay(
                            ep,
                            episodes.slice(i + 1).map((item) => ({
                              title: item.title,
                              audioUrl: item.audioUrl,
                            })),
                          )
                        }
                      />
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export async function getStaticPaths() {
  const paths = loadAllPodcastIds().map((podcast) => ({
    params: { podcast },
  }));

  return {
    paths,
    fallback: false,
  };
}

export async function getStaticProps({
  params,
}: { params: { podcast: string } }) {
  return loadDataForPodcastPage(params.podcast, process.env.STORAGE_URL ?? '');
}

export default withRouter(Podcast);
