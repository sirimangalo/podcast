import Head from 'next/head';
import Router from 'next/router';
import React from 'react';
import 'bulma/css/bulma.min.css';
import '../styles/globals.css';
import 'nprogress/nprogress.css';
import ScrollButton from '@/components/ScrollButton';
import { unescape as unescapeHe } from 'he';
import NProgress from 'nprogress';

Router.events.on('routeChangeStart', () => NProgress.start());
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());

import type { Episode, PlayingEpisode } from '@/interfaces';
import { getSmallArtworkUrl } from '@/lib/utils';
import type { AppProps } from 'next/app';
import { Player } from 'shikwasa';

type State = {
  playingEpisode: PlayingEpisode;
  nextEpisodes: Pick<Episode, 'title' | 'audioUrl'>[];
};

class PodcastBlog extends React.Component<AppProps, State> {
  private playerRef: React.RefObject<HTMLDivElement>;

  constructor(props: AppProps) {
    super(props);
    this.playerRef = React.createRef();
  }

  state = {
    playingEpisode: null,
    nextEpisodes: [],
  };

  handlePlayEpisode = (
    playingEpisode: PlayingEpisode,
    nextEpisodes: Pick<Episode, 'title' | 'audioUrl'>[] = [],
  ) => {
    this.setState({ playingEpisode, nextEpisodes }, () => {
      const title = unescapeHe(playingEpisode?.title ?? '');
      const artist = unescapeHe(playingEpisode?.podcastTitle ?? '');
      const cover = getSmallArtworkUrl(playingEpisode.podcastCoverUrl);
      const src = playingEpisode?.audioUrl ?? '';

      const player = new Player({
        container: () => this.playerRef.current,
        audio: { title, artist, cover, src },
        fixed: { type: 'fixed', position: 'bottom' },
        theme: 'dark',
        autoplay: true,
        themeColor: '#ffa500',
        download: true,
      });

      player.on('ended', () => {
        if (!this.state?.nextEpisodes?.length) {
          player.destroy();
          return;
        }

        const title = unescapeHe(this.state.nextEpisodes[0]?.title ?? '');
        const src = this.state.nextEpisodes[0]?.audioUrl ?? '';

        player.update({ cover, artist, title, src });
        player.play();

        this.setState(({ playingEpisode, nextEpisodes }) => ({
          playingEpisode: {
            ...playingEpisode,
            ...nextEpisodes[0],
          },
          nextEpisodes: nextEpisodes.slice(1),
        }));
      });

      player.play();
    });
  };

  render() {
    const { playingEpisode } = this.state;
    const { Component, pageProps } = this.props;

    return (
      <>
        <Head>
          <title>Sirimangalo Audio Library and Podcasts</title>
          <meta name="viewport" content="width=device-width, initial-scale=1" />
        </Head>

        <Component {...pageProps} onPlay={this.handlePlayEpisode} />

        <footer id="footer">
          <a href="mailto:podcast@sirimangalo.org">Contact</a>
          &nbsp;&bull;&nbsp;
          <a
            href="https://gitlab.com/sirimangalo/podcast/-/wikis/privacy"
            target="_blank"
            rel="noopener noreferrer"
          >
            Privacy Policy
          </a>
        </footer>

        <div className="player" ref={this.playerRef} />

        <ScrollButton isPlaying={!!playingEpisode} />
      </>
    );
  }
}

export default PodcastBlog;
