import LatestEpisodesList from '@/components/LatestEpisodesList';
import { unescape as unescapeHe } from 'he';
import cloneDeep from 'lodash/cloneDeep';
import omit from 'lodash/omit';
import reverse from 'lodash/reverse';
import sortBy from 'lodash/sortBy';
import Head from 'next/head';
import Link from 'next/link';
import React from 'react';

import type {
  EpisodeWithPodcastMetadata,
  Podcast,
  PodcastMetadata,
} from '../interfaces';
import {
  COUNT_LATEST_EPISODES,
  MAX_SHORT_DESC_SIZE,
  PODCAST_GROUPS,
  PODCAST_GROUP_NAMES,
  SORT_ASC,
  SORT_BY_DATE,
  SORT_BY_TITLE,
  SORT_DESC,
} from '../lib/constants';
import { loadAllPodcasts } from '../lib/data';
import {
  getLanugageName,
  getSmallArtworkUrl,
  latestEpisodeComparator,
  parsePubDate,
} from '../lib/utils';

type State = {
  searchActive: boolean;
  currentSearch: string;
  showDescriptions: Set<string>;
  filterLanguage: string;
  sortByKey: (typeof SORT_BY_TITLE)[number];
  sortOrder: (typeof SORT_ASC)[number];
};

type Props = {
  groupedPodcasts: Record<
    (typeof PODCAST_GROUPS)[number]['id'],
    PodcastMetadata[]
  >;
  podcastLanguages: string[];
  latestEpisodes: EpisodeWithPodcastMetadata[];
};

class PodcastBlog extends React.Component<Props, State> {
  state = {
    searchActive: false,
    currentSearch: '',
    showDescriptions: new Set<string>(),
    filterLanguage: '',
    sortByKey: SORT_BY_TITLE,
    sortOrder: SORT_ASC,
  };

  handleToggleDescription = (podcastId: string) =>
    this.setState(({ showDescriptions }) => {
      if (showDescriptions.has(podcastId)) {
        showDescriptions.delete(podcastId);
      } else {
        showDescriptions.add(podcastId);
      }

      return {
        showDescriptions,
      };
    });

  render() {
    const {
      searchActive,
      currentSearch,
      filterLanguage,
      showDescriptions,
      sortOrder,
      sortByKey,
    } = this.state;

    const { groupedPodcasts, podcastLanguages, latestEpisodes } = this.props;

    const filteredGroupedPodcasts = cloneDeep(groupedPodcasts);

    // apply sorting & filtering
    let numberOfShownPodcasts = 0;
    Object.entries(filteredGroupedPodcasts).forEach(([groupId, podcasts]) => {
      podcasts = sortBy(podcasts, [sortByKey]);

      if (sortOrder === SORT_DESC) {
        podcasts = reverse(podcasts);
      }

      if (filterLanguage) {
        podcasts = podcasts.filter((p) =>
          p.language.startsWith(filterLanguage),
        );
      }

      if (currentSearch) {
        podcasts = podcasts.filter((x) =>
          x.title.toLowerCase().includes(currentSearch.toLowerCase()),
        );
      }

      if (podcasts.length === 0) {
        delete filteredGroupedPodcasts[groupId];
      } else {
        filteredGroupedPodcasts[groupId] = podcasts;
        numberOfShownPodcasts += podcasts.length;
      }
    });

    return (
      <div id="home">
        <Head>
          <meta
            name="description"
            content="Collection of audio of Dhamma talks, Sutta studies, talks on the Dhammapada series, Meditation instructions, and Q&A by Yuttadhammo Bhikkhu."
          />
        </Head>

        <div className="header-section">
          <div className="columns is-vcentered">
            <div className="column is-one-quarters">
              <a
                href="https://www.sirimangalo.org/"
                target="_blank"
                rel="noreferrer noopener"
              >
                <img
                  alt="sirimangalo.org"
                  src={`${process.env.NEXT_PUBLIC_URL_BASE}/img/logo.png`}
                />
              </a>
            </div>
            <div className="column is-three-quarters">
              <p>
                This page contains collections of audio of Dhamma talks, Sutta
                studies, talks on the Dhammapada series, Meditation
                instructions, and Q&A by Yuttadhammo Bhikkhu.
              </p>
            </div>
          </div>
        </div>
        <div className="header-section-small" />

        <div className="main-content">
          {/* FILTER OPTIONS & SEARCH */}
          <div className="container">
            <div className="columns">
              <div className="column">
                <div className="filter-options">
                  <div className="field">
                    <label className="label" htmlFor="chooseLanguage">
                      Language
                    </label>
                    <div className="control select">
                      <select
                        id="chooseLanguage"
                        onChange={(e) =>
                          this.setState({ filterLanguage: e.target.value })
                        }
                        value={filterLanguage}
                      >
                        <option value="">All</option>
                        {podcastLanguages.map((langCode) => (
                          <option value={langCode} key={langCode}>
                            {getLanugageName(langCode)}
                          </option>
                        ))}
                      </select>
                    </div>
                  </div>
                  <div className="field">
                    <label className="label" htmlFor="chooseSortBy">
                      Sort By
                    </label>
                    <div className="control select">
                      <select
                        id="chooseSortBy"
                        onChange={(e) =>
                          this.setState({ sortByKey: e.target.value })
                        }
                        value={sortByKey}
                      >
                        <option value={SORT_BY_TITLE}>Name</option>
                        <option value={SORT_BY_DATE}>Last Episode Date</option>
                      </select>
                    </div>
                  </div>
                  <div className="field">
                    <label className="label" htmlFor="chooseSortOrder">
                      Sort Order
                    </label>
                    <div className="control select">
                      <select
                        id="chooseSortOrder"
                        onChange={(e) =>
                          this.setState({ sortOrder: e.target.value })
                        }
                        value={sortOrder}
                      >
                        <option value={SORT_ASC}>Ascending</option>
                        <option value={SORT_DESC}>Descending</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div className="column">
                <div className="field">
                  <label className="label" htmlFor="podcastSearch">
                    Search
                  </label>
                  <input
                    id="podcastSearch"
                    className="input"
                    placeholder="Search Podcasts"
                    type="text"
                    value={currentSearch}
                    onFocus={() => this.setState({ searchActive: true })}
                    onBlur={() => this.setState({ searchActive: false })}
                    onChange={(e) =>
                      this.setState({ currentSearch: e.target.value })
                    }
                  />
                </div>
              </div>
            </div>
          </div>

          {/* LATEST EPISODES */}
          <LatestEpisodesList
            data={latestEpisodes}
            collapsed={searchActive || currentSearch.length > 0}
          />

          {/* PODCAST LIST */}
          <div className="container mt-5">
            <span>{numberOfShownPodcasts} Podcasts</span>
          </div>

          {Object.entries(filteredGroupedPodcasts).map(
            ([groupId, podcasts]) => (
              <div key={`group-${groupId}`}>
                <div className="container podcast-group">
                  <hr />
                  <h1 id={groupId} className="podcast-group-title">
                    {PODCAST_GROUP_NAMES[groupId]}
                    <a href={`#${groupId}`}>
                      <i className="material-icons">link</i>
                    </a>
                  </h1>
                  <div className="columns is-multiline">
                    {podcasts.map((podcast) => {
                      const title = unescapeHe(podcast.title);
                      const description = unescapeHe(podcast.description);
                      const showFullDesc = showDescriptions.has(podcast.id);

                      return (
                        <div
                          key={`listitem-${podcast.id}`}
                          className="column is-half-tablet is-half-desktop is-one-third-widescreen podcast-list-item"
                        >
                          <div className="columns is-flex-mobile">
                            <div className="column artwork">
                              <Link
                                prefetch={false}
                                href="/[podcast]"
                                as={`/${podcast.id}`}
                              >
                                <div>
                                  <img
                                    src={getSmallArtworkUrl(podcast.imageUrl)}
                                    alt={title}
                                  />
                                </div>
                              </Link>
                              <div className="subtitle is-hidden-mobile">
                                {podcast.numEpisodes} episodes &nbsp;|&nbsp;
                                {getLanugageName(podcast.language)}
                              </div>
                            </div>
                            <div className="column is-one-half-tablet">
                              <Link
                                prefetch={false}
                                href="/[podcast]"
                                as={`/${podcast.id}`}
                                className="title"
                              >
                                {title}
                              </Link>
                              <p className="description">
                                <span>
                                  {description.length <= MAX_SHORT_DESC_SIZE ? (
                                    description
                                  ) : (
                                    <>
                                      {showFullDesc
                                        ? `${description} `
                                        : `${description.slice(
                                            0,
                                            MAX_SHORT_DESC_SIZE,
                                          )}... `}
                                      <a
                                        className="clickable"
                                        onClick={() =>
                                          this.handleToggleDescription(
                                            podcast.id,
                                          )
                                        }
                                      >
                                        show {showFullDesc ? 'less' : 'more'}
                                      </a>
                                    </>
                                  )}
                                </span>

                                <span className="subtitle is-block is-hidden-tablet">
                                  {podcast.numEpisodes} episodes &nbsp;|&nbsp;
                                  {getLanugageName(podcast.language)}
                                </span>
                              </p>
                            </div>
                          </div>
                        </div>
                      );
                    })}
                  </div>
                </div>
              </div>
            ),
          )}
        </div>
      </div>
    );
  }
}

export async function getStaticProps() {
  const podcastLanguages = [];
  let podcastEpisodes = [];

  const podcasts = loadAllPodcasts().map(
    ([podcastId, podcast]: [string, Podcast]) => {
      const lang = podcast.language ? podcast.language.slice(0, 2) : '';
      const lastEpisodeDate = parsePubDate(
        podcast?.episodes?.[0]?.pubDate,
      ).format();

      podcastEpisodes = [
        ...podcastEpisodes,
        ...podcast.episodes.map((episode) => ({
          ...episode,
          imageUrl: podcast.imageUrl,
          podcastName: podcast.title,
          podcastId,
        })),
      ];

      if (lang && !podcastLanguages.includes(lang)) {
        podcastLanguages.push(lang);
      }

      return {
        ...omit(podcast, ['episodes']),
        id: podcastId,
        lastEpisodeDate,
        numEpisodes: podcast.episodes.length,
      };
    },
  );

  // sort by [date, episode if same podcast] descending
  const latestEpisodes = podcastEpisodes
    .sort(latestEpisodeComparator)
    .slice(0, COUNT_LATEST_EPISODES);

  const groupedPodcasts = {};
  PODCAST_GROUPS.forEach(({ id }) => {
    groupedPodcasts[id] = [];
  });

  podcasts.forEach((podcast) => {
    const groupId = PODCAST_GROUPS.filter(({ pattern }) =>
      pattern.test(podcast.id),
    )[0].id;
    groupedPodcasts[groupId].push(podcast);
  });

  return {
    props: { groupedPodcasts, podcastLanguages, latestEpisodes },
  };
}

export default PodcastBlog;
