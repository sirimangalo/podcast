import { readFileSync, writeFileSync } from 'node:fs';
import { basename, join } from 'node:path';
import { ArgumentParser, type ArgumentParserOptions } from 'argparse';
import he from 'he';
import _ from 'lodash';
import mime from 'mime-types';
import moment from 'moment';
import sanitizeHtml from 'sanitize-html';
import XLSX from 'xlsx';
import 'dotenv/config';

import type { Episode, Podcast } from '../interfaces';

const parser = new ArgumentParser({
  add_help: true,
  description:
    'This script parses the podcast related show and episode data from a spreadsheet (Excel, OpenOffice) file and converts it into JSON. Each worksheet in the spreadsheet should fulfill the predefined table structure for providing the metadata fields/values and will generate a seperate JSON file named after the sheet in the output directory. Those then can be used for further processing like creating RSS feed files.',
} as ArgumentParserOptions);

parser.add_argument('-i', '--input', {
  required: true,
  help: 'A spreadsheet file (i.e. ".ods", ".xlsx") containing the data in the compatible table format.',
});
parser.add_argument('-m', '--metadata-file', {
  required: true,
  help: 'A JSON file containing a metadata map (size, type, guid) for the audio urls.',
});
parser.add_argument('-d', '--durations-file', {
  required: true,
  help: 'A JSON file containing a metadata map (filebasename => filesize => duration) of audio files.',
});
parser.add_argument('-o', '--output-dir', {
  required: true,
  help: 'The output directory for saving the JSON files.',
});

const args = parser.parse_args();

const AUDIO_DURATIONS = JSON.parse(readFileSync(args.durations_file, 'utf8'));
const AUDIO_METADATA = {};

Object.entries(JSON.parse(readFileSync(args.metadata_file, 'utf8'))).map(
  ([key, value]) => {
    const url = `${process.env.STORAGE_URL?.split('digitaloceanspaces.com')[0]}digitaloceanspaces.com/${encodeURI(key)}`;
    AUDIO_METADATA[url] = value;
  },
);

const wb = XLSX.readFile(args.input, {
  sheetRows: 500,
});

/**
 * Wrap string in cdata section (ref: https://www.w3.org/TR/xml/#syntax)
 */
const cdata = (a: string) => `<![CDATA[${a.replace(/\]\]\>/g, ']]&gt;')}]]>`;

const generateGuid = (url: string) =>
  decodeURIComponent(url)
    .replace(process.env.STORAGE_URL, '')
    .replace(/^\/+/g, '')
    .replace('.mp3', '');

const parseChapters = (raw: string) => {
  try {
    const lines = raw.split('\n');
    const res: { start: string; title: string; href?: string }[] = [];

    for (const line of lines) {
      const [start, rest] = line.split(/\s+(.*)/g, 2);

      // ref: https://stackoverflow.com/a/8318367
      if (
        !/^(?:(?:([01]?\d|2[0-3]):)?([0-5]?\d):)?([0-5]?\d)$/.test(start) ||
        !rest
      ) {
        continue;
      }

      res.push({
        start,
        title: he.escape(rest).trim(),
      });
    }

    return res;
  } catch (err) {
    console.error('Failed to parse chapters', err);
    return [];
  }
};

/**
 * Sanitize HTML from string
 */
const sanitize = (a: string) =>
  sanitizeHtml(a, {
    allowedTags: [
      'h3',
      'h4',
      'h5',
      'h6',
      'blockquote',
      'p',
      'a',
      'ul',
      'ol',
      'nl',
      'li',
      'b',
      'i',
      'strong',
      'em',
      'strike',
      'abbr',
      'hr',
      'br',
      'div',
      'table',
      'thead',
      'caption',
      'tbody',
      'tr',
      'th',
      'td',
    ],
    disallowedTagsMode: 'discard',
    allowedAttributes: {
      a: ['href', 'target', 'rel'],
    },
    selfClosing: ['img', 'br', 'hr'],
  });

/**
 * Parse duration into seconds.
 * Format: HH:mm:ss
 *
 * Valid: I.e. '12', '1:23', '01:32', '1:10:20'
 *
 * @param timeStr
 */
const parseDurationString = (timeStr: string): number => {
  let i = 0;
  let seconds = 0;

  const reInt = /^[0-9]{1,2}$/;

  for (const part of timeStr.split(':').reverse()) {
    const s = part.trim();

    if (i > 2 || !reInt.test(s)) {
      throw new Error('Parse Error');
    }

    const t = Number.parseInt(s, 10);
    if (!(t >= 0 && t < 60)) {
      throw new Error('Parse Error');
    }

    seconds += t * 60 ** i;
    i++;
  }

  return seconds;
};

/**
 * Parse episode list from sheet.
 */
const getEpisodes = (sheet: XLSX.Sheet, sheetName: string): Episode[] => {
  const EPISODE_START = 'EPSIODES';
  const COL_PUBLISHED = 'B';
  const COL_TITLE = 'C';
  const COL_SUBTITLE = 'D';
  const COL_DESC = 'E';
  const COL_CONTENT_ENCODED = 'F';
  const COL_AUDIO_URI = 'G';
  const COL_PUB_DAY = 'H';
  const COL_PUB_TIME = 'I';
  const COL_CATEGORIES = 'J';
  const COL_EPISODE = 'K';
  const COL_SEASON = 'L';
  const COL_DURATION = 'M';
  const COL_CHAPTERS = 'N';

  // Find the marker indicating the start of the episode table
  const eStart = _.entries(sheet).find(
    ([cell, { v }]) =>
      cell.startsWith('A') &&
      typeof v === 'string' &&
      v.trim().toUpperCase() === EPISODE_START,
  );

  if (!eStart) {
    throw new Error('Could not find start row for episodes.');
  }

  // Get the row number of the header
  const headerRow = Number.parseInt(eStart[0].slice(1), 10);
  if (Number.isNaN(headerRow)) {
    throw new Error('Could not parse start row for episodes.');
  }

  // Parse the episode list
  const episodes: Episode[] = [];

  for (let i = headerRow + 1; i <= headerRow + 300; i++) {
    if (_.get(sheet, `${COL_PUBLISHED}${i}.v`, '') !== true) {
      continue;
    }

    const day = _.get(sheet, `${COL_PUB_DAY}${i}.w`, '').trim();
    const time = _.get(sheet, `${COL_PUB_TIME}${i}.v`, 0);

    const audioUrl = _.get(sheet, `${COL_AUDIO_URI}${i}.v`, '').trim();

    if (typeof AUDIO_METADATA[audioUrl] === 'undefined') {
      console.error(`ERROR: No metadata for ${audioUrl}`);
      process.exit(1);
    }

    const metadata = AUDIO_METADATA[audioUrl];
    const guid = generateGuid(audioUrl);

    const audioSize = metadata.Size;
    const audioType = mime.lookup(audioUrl);

    const pubDate = moment
      .utc(`${day} ${time}`, 'YYYY-MM-DD HH:mm:ss', true)
      .format('ddd, DD MMM YYYY HH:mm:ss ZZ');
    const categories = _.get(sheet, `${COL_CATEGORIES}${i}.v`, '')
      .split(',')
      .map((s) => s.trim())
      .filter((s) => s);

    const title = _.get(sheet, `${COL_TITLE}${i}.v`, '').trim();

    const subtitle = _.get(sheet, `${COL_SUBTITLE}${i}.v`, '').trim();
    const description = _.get(sheet, `${COL_DESC}${i}.v`, '').trim();
    const contentEncoded = _.get(
      sheet,
      `${COL_CONTENT_ENCODED}${i}.v`,
      '',
    ).trim();
    const episode = _.get(sheet, `${COL_EPISODE}${i}.v`, '');
    const season = _.get(sheet, `${COL_SEASON}${i}.v`, '');

    const reFriendlyGuid = /^[A-Za-z0-9_-]{3,80}$/;
    const guidNoSlash = guid.replace(/\//g, '_');
    const link = reFriendlyGuid.test(guidNoSlash)
      ? `${process.env.NEXT_PUBLIC_URL_BASE}/${sheetName}/#${guidNoSlash}`
      : '';

    let duration: number;
    try {
      const durationStr = _.get(sheet, `${COL_DURATION}${i}.v`, '');

      if (durationStr) {
        if (/^[0-9]+$/.test(durationStr)) {
          duration = Number.parseInt(durationStr);
        } else {
          duration = parseDurationString(durationStr);
        }
      }
    } catch (err) {
      console.warn('Could not parse duration');
    }

    if (!duration) {
      const durationFromDurationsFile = _.get(
        AUDIO_DURATIONS[basename(audioUrl)],
        audioSize.toString(),
        0,
      );
      if (durationFromDurationsFile > 0) {
        duration = durationFromDurationsFile;
      }
    }

    if (
      Number.isNaN(duration) ||
      typeof duration !== 'number' ||
      duration <= 0
    ) {
      console.warn(`No duration for ${audioUrl}`);
    }

    const chapters = parseChapters(_.get(sheet, `${COL_CHAPTERS}${i}.v`, ''));

    episodes.push({
      pubDate: he.escape(pubDate),
      audioUrl: he.escape(audioUrl),
      categories: categories.map((s) => he.escape(s)),
      link: he.escape(link),
      title: he.escape(title),
      description: description ? cdata(sanitize(description)) : '',
      contentEncoded: contentEncoded ? cdata(sanitize(contentEncoded)) : '',
      subtitle: he.escape(subtitle),
      guid: he.escape(guid),
      audioSize,
      audioType: he.escape(audioType),
      ...(!Number.isNaN(duration) &&
        typeof duration === 'number' && { duration }),
      ...(!Number.isNaN(season) && typeof season === 'number' && { season }),
      ...(!Number.isNaN(episode) && typeof episode === 'number' && { episode }),
      ...(chapters && chapters.length > 0 && { chapters }),
    });
  }

  return episodes;
};

/**
 * Parse channel information.
 */
const getChannel = (sheet: XLSX.Sheet, sheetName: string): Podcast | null => {
  const CHANNEL_START = 'CHANNEL';
  const COL_PUBLISHED = 'B';
  const COL_TITLE = 'C';
  const COL_DESC = 'D';
  const COL_IMAGE_URI = 'E';
  const COL_CATEGORY = 'F';
  const COL_SUBCATEGORY = 'G';
  const COL_AUTHOR = 'H';
  const COL_LANGUAGE = 'I';
  const COL_OWNER_NAME = 'J';
  const COL_OWNER_EMAIL = 'K';
  const COL_SERIAL = 'L';
  const COL_COMPLETE = 'M';
  const COL_ITUNES_LINK = 'N';
  const COL_SPOTIFY_LINK = 'O';
  const COL_GOOGLE_LINK = 'P';

  // Find the marker indicating the start of the episode table
  const eStart = _.entries(sheet).find(
    ([cell, { v }]) =>
      cell.startsWith('A') &&
      typeof v === 'string' &&
      v.trim().toUpperCase() === CHANNEL_START,
  );

  if (!eStart) {
    throw new Error('Could not find start row for channel.');
  }

  // Get the row number of the header
  const headerRow = Number.parseInt(eStart[0].slice(1), 10);
  if (Number.isNaN(headerRow)) {
    throw new Error('Could not find start row for channel.');
  }

  const i = headerRow + 1;
  if (_.get(sheet, `${COL_PUBLISHED}${i}.v`, false) !== true) {
    return null;
  }

  return {
    ownerName: he.escape(_.get(sheet, `${COL_OWNER_NAME}${i}.v`, '')),
    ownerEmail: he.escape(_.get(sheet, `${COL_OWNER_EMAIL}${i}.v`, '')),
    link: he.escape(`${process.env.NEXT_PUBLIC_URL_BASE}/${sheetName}/`),
    atomLink: he.escape(
      `${process.env.NEXT_PUBLIC_URL_BASE}/${sheetName}/rss.xml`,
    ),
    title: he.escape(_.get(sheet, `${COL_TITLE}${i}.v`, '').trim()),
    description: he.escape(_.get(sheet, `${COL_DESC}${i}.v`, '')),
    imageUrl: he.escape(_.get(sheet, `${COL_IMAGE_URI}${i}.v`, '')),
    category: he.escape(_.get(sheet, `${COL_CATEGORY}${i}.v`, '')),
    subcategory: he.escape(_.get(sheet, `${COL_SUBCATEGORY}${i}.v`, '')),
    author: he.escape(_.get(sheet, `${COL_AUTHOR}${i}.v`, '')),
    language: he.escape(_.get(sheet, `${COL_LANGUAGE}${i}.v`, 'en')),
    type:
      _.get(sheet, `${COL_SERIAL}${i}.v`, false) === true
        ? 'serial'
        : 'episodic',
    complete:
      _.get(sheet, `${COL_COMPLETE}${i}.v`, false) === true ? 'Yes' : 'No',
    itunesLink: _.get(sheet, `${COL_ITUNES_LINK}${i}.v`, ''),
    spotifyLink: _.get(sheet, `${COL_SPOTIFY_LINK}${i}.v`, ''),
    googleLink: _.get(sheet, `${COL_GOOGLE_LINK}${i}.v`, ''),
    episodes: [],
  };
};

/**
 * Try to parse each sheet in the workbook and
 * create the data structure for a podcast that
 * is saved to json for further processing.
 */
for (const sheetName of wb.SheetNames) {
  console.log(`Trying to parse "${sheetName}"`);

  try {
    const sheet = wb.Sheets[sheetName];
    const channel = getChannel(sheet, sheetName);

    if (channel !== null) {
      channel.episodes = getEpisodes(sheet, sheetName).reverse();

      console.log(`Successfully parsed "${sheetName}"`);
      writeFileSync(
        join(args.output_dir, `${sheetName}.json`),
        JSON.stringify(channel, null, 2),
      );
    } else {
      console.log(`"${sheetName}" is not marked as published`);
    }
  } catch (e) {
    console.error(sheetName, e);
  }
}
